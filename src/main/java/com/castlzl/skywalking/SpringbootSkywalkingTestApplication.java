package com.castlzl.skywalking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSkywalkingTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSkywalkingTestApplication.class, args);
    }

}
