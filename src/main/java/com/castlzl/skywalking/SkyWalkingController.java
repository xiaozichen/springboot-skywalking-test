package com.castlzl.skywalking;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author leiziliang
 * @version 1.0
 * @date 2021/4/18 下午3:26
 */
@RestController
@RequestMapping("/test")
public class SkyWalkingController {

    @GetMapping("/hello")
    public String hello() {
        return "this is springboot skywalking test";
    }
}
